library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm is
  port
  (
    outp : out std_logic;
    clk : in std_logic;
    duty_ticks : in unsigned(31 downto 0);
    period_ticks : in unsigned(31 downto 0)
  );
end pwm;

architecture pwm_arch of pwm is
begin
  pwm_p: process(clk)
    variable cnt : unsigned(31 downto 0) := (others => '0');
  begin
    if(rising_edge(clk)) then
      if(cnt < duty_ticks) then
        cnt := cnt + 1;
        outp <= '1';
      elsif(cnt < period_ticks) then
        cnt := cnt + 1;
        outp <= '0';
      else
        outp <= '0';
        cnt := x"00000000";
      end if;
    end if;
  end process;
end pwm_arch;
