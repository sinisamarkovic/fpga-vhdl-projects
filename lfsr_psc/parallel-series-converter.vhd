library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity psc is
    port(
        clk: in std_logic;
        pdata0,pdata1,pdata2,pdata3,pdata4,pdata5,pdata6,pdata7 : in std_logic;
        pclk: in std_logic;
        xdata: out std_logic;
        xclk: out std_logic
    );
end entity;

architecture arch_psc of psc is
    signal output_clk : std_logic := '0';
    signal output0,output1,output2,output3,output4,output5,output6,output7 : std_logic;
begin
    xclk <= output_clk;
    xdata <= output0;
    process(output_clk)
        variable i : integer := 0;
    begin
        if rising_edge(output_clk) then
            if i = 0 then
                output0 <= pdata0;
                output1 <= pdata1;
                output2 <= pdata2;
                output3 <= pdata3;
                output4 <= pdata4;
                output5 <= pdata5;
                output6 <= pdata6;
                output7 <= pdata7;
                i := i + 1;
            elsif i = 7 then
                i := 0;
                output0 <= output1;
                output1 <= output2;
                output2 <= output3;
                output3 <= output4;
                output4 <= output5;
                output5 <= output6;
                output6 <= output7;
            else
                output0 <= output1;
                output1 <= output2;
                output2 <= output3;
                output3 <= output4;
                output4 <= output5;
                output5 <= output6;
                output6 <= output7;
                i := i + 1;
            end if;
        end if;
    end process;
    process(clk) is
        variable i : integer := 0;
    begin
        if rising_edge(clk) then
            if i < 3125000 then
                output_clk <= '1';
                i := i + 1;
            elsif i >= 3125000 and i < 6250000 then
                output_clk <= '0';
                i := i + 1;
            else
                i := 0;
            end if;
        end if;
    end process;
end architecture;