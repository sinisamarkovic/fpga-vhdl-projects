library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity spi_profesor is
port( iclk: in std_logic;
	mosi: out std_logic:='L';
	miso: in std_logic;
	sck: out std_logic:='L';
	cs: out std_logic:='H';
	
	wdata: in unsigned (7 downto 0);
	wi: in std_logic;
	wc: out std_logic:='0';
	rdata: out unsigned (7 downto 0) := x"00";
	ri: in std_logic;
	rc: out std_logic:='0');
end spi_profesor;

architecture a_spi of spi_profesor is
signal txdata: unsigned (7 downto 0) := x"00";
signal rxdata: unsigned (7 downto 0) := x"00";
signal pomoc : std_logic := '0';
begin

write: process(iclk)
		variable state:integer:=0;
		variable cnt:integer:=0;
		variable idx:integer:=0;
		variable txflag:integer:= 0;
		variable halfrate:integer:=25;			--> 1MHz -> 100 clocks of 100MHz	
		variable pause:integer:=10;
		variable randpause : integer := 0;

		
		
		begin
			if iclk'event and iclk='1' then
				if wi = '1' and txflag = 0 then
					txdata <= wdata;
					wc <= '0';
					txflag := 1;
					idx := 8;
					state := 0;
					cnt := 0;
					cs <= '0';
				end if;
				
			
				if txflag = 1 then
					cnt := cnt + 1;
					if(cnt = 1 and randpause = 0) then
						cnt := 0;
						randpause := 1;
					elsif(state = 0) then
						state := 1;
						sck <= '0';
						mosi <= txdata(idx-1);
					elsif(state = 1) then
						if(cnt >= halfrate) then
							cnt := 0;
							state := 2;
							sck <= '1';
						end if;
					elsif(state = 2) then
						if(cnt >= halfrate) then
							cnt := 0;
							state := 0;
							idx := idx - 1;
							sck <= '0';
							if(idx = 0) then
								state := 3;
								cs <= 'H';
								sck <= 'L';
								mosi <= 'L';
								wc <= '1';
							end if;
						end if;
					else
						if(cnt = pause) then
							wc <= '1';
							cnt := 0;
							state := 0;
							txflag := 0;
							randpause := 0;
						end if;	
					end if;
				end if;
			end if;	
	end process;	

read: process(iclk) 
		
		variable state:integer:=0;
		variable cnt:integer:=0;
		variable idx:integer:=0;
		variable rxflag:integer:= 0;
		variable halfrate:integer:=25;			--> 1MHz -> 100 clocks of 100MHz	
		variable pause:integer:=10;
		
		begin
			if iclk'event and iclk='1' then
				if ri = '1' and rxflag = 0 then
					rxdata <= x"00";
					rc <= '0';
					rxflag := 1;
					idx := 8;
					state := 0;
					cnt := 0;
					cs <= '0';
				end if;
				
			
				if rxflag = 1 then
					cnt := cnt + 1;
					
					if(state = 0) then
						state := 1;
						sck <= '0';
					elsif(state = 1) then
						if(cnt >= halfrate) then
							cnt := 0;
							state := 2;
							sck <= '1';
							rxdata(idx-1) <= miso;
						end if;
					elsif(state = 2) then
						if(cnt >= halfrate) then
							cnt := 0;
							state := 0;
							idx := idx - 1;
							sck <= '0';
							if(idx = 0) then
								state := 3;
								cs <= 'H';
								sck <= 'L';
								mosi <= 'L';
								rc <= '1';
							end if;
						end if;
					else
						if(cnt = pause) then
							rc <= '1';
							cnt := 0;
							state := 0;
							rxflag := 0;
						end if;	
					end if;
				end if;
			end if;	
	end process;
	rdata <= rxdata;

end a_spi;

