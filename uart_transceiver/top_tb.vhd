library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture arctop_tb of top_tb is
component top is
  Port (  dtx : out std_logic;	-- uart tx pin	
			drx : in std_logic;		-- uart rx pin	
			fclk: in  std_logic 	-- fpga clock pin
  );		
end component;
signal fclk : std_logic := '0';
signal drx : std_logic := '1';
signal dtx : std_logic;
begin
  inst_top: top port map (fclk=>fclk, drx=>drx, dtx=>dtx);
  process
  begin
    fclk <= '1';
    wait for 10 ns;
    fclk <= '0';
    wait for 10 ns;
    fclk <= '1';
  end process;
  process
  begin
    wait for 100 ns;
    drx <= '0';
    wait for 2400 ns; --65
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 23600 ns;
    drx <= '0';
    wait for 2400 ns; --97
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 8680 ns;
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 23600 ns; 
    drx <= '0';
    wait for 2400 ns; --40
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '0';
    wait for 8680 ns;
    drx <= '1';
    wait;
  end process;
end arctop_tb;