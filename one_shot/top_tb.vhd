library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture arctop_tb of top_tb is
component top is
  Port (  one_shot : in std_logic;	
			one_shot_output : out std_logic;		
			fclk: in  std_logic; 	
			one_shot_delay : in unsigned(31 downto 0) := (others => '0');
			one_shot_width : in unsigned(31 downto 0) := (others => '0')
	);		
end component;
signal one_shot : std_logic := '0';
signal one_shot_output : std_logic := '0';
signal fclk : std_logic := '0';
signal one_shot_delay : unsigned(31 downto 0) := x"00000064";
signal one_shot_width : unsigned(31 downto 0) := x"00000032";
begin
  inst_top: top port map (one_shot=>one_shot, one_shot_output=>one_shot_output, fclk=>fclk, one_shot_delay=>one_shot_delay, one_shot_width=>one_shot_width);
  process
  begin
    fclk <= '1';
    wait for 10 ns;
    fclk <= '0';
    wait for 10 ns;
    fclk <= '1';
  end process;
  process
  begin
    wait for 10 ns;
    one_shot <= '1';
    wait for 150 us;
    one_shot <= '0';
    wait for 20 us;
  end process;
end arctop_tb;