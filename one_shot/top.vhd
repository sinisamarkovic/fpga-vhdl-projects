library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top is
	Port (  one_shot : in std_logic;	
			one_shot_output : out std_logic;		
			fclk: in  std_logic; 	
			one_shot_delay : in unsigned(31 downto 0) := (others => '0');
			one_shot_width : in unsigned(31 downto 0) := (others => '0')
	);		
end top;

architecture arctop of top is
begin
  proc_one_shot: process(fclk, one_shot)
    variable state : unsigned (7 downto 0) := x"00";
    variable micro_cnt : unsigned (7 downto 0) := x"00";
    variable cnt : unsigned (35 downto 0) := x"000000000";
  begin
    if(rising_edge(one_shot)) then
      state := x"01";
      micro_cnt := x"00";
    end if;
    if(rising_edge(fclk)) then
      if(state > x"00") then
        if(micro_cnt = x"32") then
          micro_cnt := x"00";
          cnt := cnt + 1;
        end if;
      end if;
      if(state = x"00") then
        one_shot_output <= '0';
      elsif(state = x"01") then
        if(cnt = one_shot_delay) then
          state := x"02";
        end if;
        one_shot_output <= '0';
      elsif(state = x"02") then
        if(cnt = one_shot_delay + one_shot_width) then
          state := x"00";
          micro_cnt := x"00";
          cnt := x"000000000";
        end if;
        one_shot_output <= '1';
      end if;
      micro_cnt := micro_cnt + 1;
    end if;
  end process;
end arctop;