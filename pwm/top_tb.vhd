library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture arctop_tb of top_tb is

component top
	port( 
	ipin:  out std_logic := '0';
	ipin_inv: out std_logic := '1';
  iclk: in std_logic;
  btn: in std_logic := '0');
end component;
	signal ipin:  std_logic;
	signal ipin_inv: std_logic;
  signal iclk: std_logic := '0';
  signal btn: std_logic := '0';
begin
  inst_top: top port map (ipin=>ipin, ipin_inv=>ipin_inv, iclk=>iclk, btn=>btn);
  clk: process
  begin
    iclk <= '1';
    wait for 10 ns;
    iclk <= '0';
    wait for 10 ns;
  end process;
  button: process
  begin
    wait for 1 ms;
    btn <= '1';
    wait for 20 ns;
    btn <= '0';
    wait for 1 ms;
    btn <= '1';
    wait for 20 ns;
    btn <= '0';
    wait for 1 ms;
    btn <= '1';
    wait for 20 ns;
    btn <= '0';
    wait for 1 ms;
    btn <= '1';
    wait for 20 ns;
    btn <= '0';
    wait for 1 ms;
    btn <= '1';
    wait for 20 ns;
    btn <= '0';
    wait for 1 ms;
    wait;
  end process;
end arctop_tb;



