library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top is
	port (	
		ipin:  out std_logic := '0';
		ipin_inv: out std_logic := '1';
		iclk: in std_logic;
		btn: in std_logic := '0'
	);
end top;

architecture arctop of top is

component pwm
	port( 
	ipin:  out std_logic := '0';
	ipin_inv: out std_logic := '1';
	iclk: in std_logic;
	iduty: in unsigned (31 downto 0) := x"00000000";
	ifreq: in unsigned (31 downto 0) := x"00000000");
end component;
signal ifreq : unsigned(31 downto 0) := x"0000c350";
signal iduty : unsigned(31 downto 0) := x"00000000";
begin
	-- instanciranje komponenti
	inst_pwm: pwm port map (ipin=>ipin, ipin_inv=>ipin_inv, iclk=>iclk, ifreq=>ifreq, iduty=>iduty);
	process(btn,iclk)
		variable inc : unsigned(31 downto 0) := x"000009c4";
	begin
		if btn'event and btn = '0' then
			if iduty /= ifreq then
				iduty <= iduty + inc;
			else
				iduty <= x"00000000";
			end if;
		end if;
	end process;
end arctop;



