library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity pwm is
	port( 
	ipin:  out std_logic := '0';
	ipin_inv: out std_logic := '1';
	iclk: in std_logic;
	iduty: in unsigned (31 downto 0) := x"00000000";
	ifreq: in unsigned (31 downto 0) := x"00000000");
end pwm;

architecture a_pwm of pwm is
signal cnt: unsigned (31 downto 0) := x"00000000";

begin
	process(iclk)
	
	begin
		if iclk'event and iclk='1' then
			if cnt<iduty then
				cnt<=cnt+1;
				ipin<='1';
				ipin_inv <= '0';
			elsif cnt<ifreq then
				cnt<=cnt+1;
				ipin<='0';
				ipin_inv <= '1';
			else
				cnt<=x"00000000";
				ipin<='0';
				ipin_inv <= '1';
			end if;
	    end if;
	end process;	
end a_pwm;

